<?php
/**
* @file
* Rate Content by custom criterias
*/


define('CRITERIA_RATE_ACCESS_VOTE_CONTENT', 'criteria rate on content');

/**
 * Implementation of hook_perm().
 */
function criteria_rating_perm() {
  return array(CRITERIA_RATE_ACCESS_VOTE_CONTENT);
}

/**
 * Implementation of hook_menu().
 */
function criteria_rating_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'criteria_rating/vote',
      'callback' => 'criteria_rating_vote',
      'type' => MENU_CALLBACK,
      'access' => user_access(CRITERIA_RATE_ACCESS_VOTE_CONTENT),
    );
    $items[] = array(
      'path' => 'criteria_rating/unvote',
      'callback' => 'criteria_rating_unvote',
      'type' => MENU_CALLBACK,
      'access' => user_access(CRITERIA_RATE_ACCESS_VOTE_CONTENT),
    );
    $items[] = array(
      'path' => 'criteria_rating/recalculate',
      'callback' => '_criteria_rating_table_update',
      'type' => MENU_CALLBACK,
      'access' => user_access(CRITERIA_RATE_ACCESS_VOTE_CONTENT),
    );
  }
  return $items;
}

/**
 * Called by jQuery.
 * Submits the vote request and returns JSON to be parsed by jQuery.
 */
function criteria_rating_vote($nid, $tag, $value, $type='percent' ) {
  global $user;

  // Before processing the vote we check that the user is logged in, we have a node ID
  if ($user->uid && ($nid > 0)  && user_access(CRITERIA_RATE_ACCESS_VOTE_CONTENT) ) {
    $vote = new stdClass;
    $vote->value = max(0, min(100, $value));
    $vote->value_type = $type;
    $vote->tag = arg(3);
    
    //store vote
    $vote->value ? votingapi_set_vote('node', $nid, $vote) : votingapi_unset_vote('node', $nid);
    watchdog('criteria_rating', t('Vote by @user accepted', array('@user' => $user->name)));

    if ($votes = criteria_rating_get_user_votes('node', $nid, $user->uid, array($tag) ) ) {
      foreach ($votes as $vote) {
        if ($vote->value_type == 'percent' ) {
          $rating = $vote;
          $given_vote= $vote->value;
          break;
        }
      }
    }
    // This print statement will return results to jQuery's request.
    print drupal_to_js( array (
      'score' => $given_vote,
      'voted' => t('You voted'),
      'tag' => $tag,
      )
    );
  }
  exit();
}

function criteria_rating_unvote ($nid, $tag, $value, $type='percent' ) {
  global $user;
  // Before processing the vote we check that the user is logged in,
  // we have a node ID
  if ($user->uid && ($nid > 0)  && user_access(CRITERIA_RATE_ACCESS_VOTE_CONTENT) ) {
    $vote_id = criteria_rating_votingapi_get_vote_by_tag($nid, arg(3), $user->uid);
    db_query("DELETE FROM {votingapi_vote} WHERE vote_id=%d", $vote_id );
    watchdog('criteria_rating', t('UnVote by @user accepted', array('@user' => $user->name)));
    // This print statement will return results to jQuery's request.
    print drupal_to_js(array(
      'unvoted' => 1,
      )
    );
  }
  exit();
}

/**
 * A simple helper function that returns all votes cast by a given user for a piece of content.
 *
 * @param $content_type
 *   A string identifying the type of content whose votes are being retrieved. Node, comment, aggregator item, etc.
 * @param $content_id
 *   The key ID of the content whose votes are being retrieved.
 * @param $uid
 *   The integer uid of the user whose votes should be retrieved.
 * @param $tag
 *   Criteria tag for the vote
 * @return
 *   An array of matching votingapi_vote records.
 */
function criteria_rating_get_user_votes($content_type, $content_id, $uid = NULL, $tag) {
  if ($uid == NULL) {
    global $user;
    $uid = $user->uid;
  }
  return _votingapi_get_raw_votes($content_type, $content_id, NULL, $tag, $uid);
}

/**
 * Create voting widget to display on the webpage.
 */
function criteria_rating_jquery_widget($node) {
  global $user;
  drupal_add_css(drupal_get_path('module', 'criteria_rating') .'/criteria_rating.css');
  drupal_add_css(drupal_get_path('module', 'criteria_rating') . '/js/jquery.rating.css');
  drupal_add_js(drupal_get_path('module', 'criteria_rating') . '/js/jquery-1.2.6.js');
  drupal_add_js(drupal_get_path('module', 'criteria_rating') . '/js/jquery.rating.js');
    
  //only for devel
  $thiserver='';

  drupal_add_js("var sys_path = " . drupal_to_js($thiserver) . ";", 'inline');    
  
  $r = criteria_rating_votables();    
  
  for ($i=1;$i<6;$i++)  {
    $tag = variable_get('criteria_rating'.$i.'_' . $node->type  , 0);
    $label = variable_get('criteria_rating_label'.$i.'_' . $node->type  , 0);
  
    if (strlen($tag)>0)  {    
      $star_list .= "<div style='clear:both' class='rating rating-".$tag ."' ><div class='rating-intro'><span>$label:</span></div> <div class='rating-stars'>";
      if ($voted = criteria_rating_get_user_votes('node', $node->nid, $uid = NULL, array($tag) ) )  {
        $vote_to_expose         = $voted[0]->value;
        $allowed_to_vote        = 1;
        $someonevotes_class     ='';    
      }
      elseif    (user_access(CRITERIA_RATE_ACCESS_VOTE_CONTENT))  {
        $someonevotes_class     = 'star_hover';        
        $voted                  = votingapi_get_voting_result('node', $node->nid, 'percent', $tag, 'average') ;
        $vote_to_expose         = $voted->value;
        $allowed_to_vote        = 1;  
      }
      else  {
        $voted                  = votingapi_get_voting_result('node', $node->nid, 'percent', $tag, 'average') ;
        $vote_to_expose         = $voted->value;
        $allowed_to_vote        = 0;  
        $who_voted              = '';
        $someonevotes_class     = '';  
      }    
      
      for  ( $ii=1; $ii<6;$ii++  ) {
        $checked='';
        if ($vote_to_expose == $r[$ii]['value'])  {
          $checked = ' checked="checked" ';
        }
                
        if ($allowed_to_vote==1)  {
          $avalue= $node->nid."-".$tag."-". $r[$ii]['value'] ."-".$r[$ii]['label'];
          $atitle = t("This"). " " . $node->type." ".t("is") ." ". $r[$ii]['label'] . " ".t("on") ." '" . $label ."'" ;
        }
        else {
          $avalue= "login-$tag-".$r[$ii]['value']."-".$r[$ii]['label'];
          $atitle = t('Please login to vote this '. $node->type) ;
          $disabled = " disabled ";
        }
          
        $star_list .= "<input id=\"".$avalue."\" name=\"star".$i."\" type=\"radio\" class=\"star \"  value=\"$atitle\" $disabled  $checked /> ";
      }
      $star_list .= "</div></div>";
    }
  }
  return theme('criteria_rating_widget', $star_list);
}

/**
 * Theme for the voting widget.
 */
function theme_criteria_rating_widget( $star_list) {
  $output  = '<div class="criteria_rating-widget">';
  $output .= '<div class="vote">';
  $output .= $star_list;
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}

/**
 * Implementation of hook_nodeapi().
 */
function criteria_rating_nodeapi(&$node, $op, $teaser, $page) {
  switch ($op) {
  case 'view':
  // Show the widget, but only if the full node is being displayed.
    if (  ( !$teaser && arg(0)=='node'  ) && variable_get('criteria_rating_enable_'. $node->type  , 0)) {
      $node->content['criteria_rating_widget'] = array(
        '#value' => criteria_rating_jquery_widget($node),
        '#weight' => 100,
      );
    }
    break;
  }
}

function criteria_rating_form_alter($form_id, &$form) {
  //setting up content types for criteria rating
  if ($form_id == 'node_type_form') {
    $form['workflow']['criteria_rating_setting'] = array(
      '#type' => 'fieldset',
      '#title' => 'Criteria Rating Setting',
      '#collapsed' => false,
      '#collapsible' => 1,
      '#weight' => 4,
      'criteria_rating_enable' => array(
        '#type' => 'checkbox',
        '#title' => t('Enable Criteria Rating for ' . $form['#node_type']->type),
        '#default_value' => variable_get('criteria_rating_enable_' . $form['#node_type']->type  , 0),
        '#weight' => 0,
      ),
    );
      
    for ( $i=1; $i<6 ; $i++)  {
      $form['workflow']['criteria_rating_setting']['group'.$i] = array(
        '#type' => 'fieldset',
        '#title' => 'Criteria ' .$i,
        '#collapsed' => false,
        '#collapsible' => 1,
        '#weight' => $i,
      );
    
      $form['workflow']['criteria_rating_setting']['group'.$i]['criteria_rating'.$i] = array(
        '#type' => 'textfield',
        '#title' => t('Tag') . ' (' .t('criteria'). ') ' . $i,
        '#default_value' => variable_get('criteria_rating'.$i.'_'. $form['#node_type']->type, 'tag'.$i),
        '#size' => 26,
        '#maxlength' => 128,
        '#weight' => 0,
        '#description' => t('Here, just lowcase chars (up to 26)'),
      );
        
      $form['workflow']['criteria_rating_setting']['group'.$i]['criteria_rating_label'.$i] = array(
        '#type' => 'textfield',
        '#title' => t('Label').' ' . $i,
        '#default_value' => variable_get('criteria_rating_label'.$i.'_'. $form['#node_type']->type, 'Label '.$i),
        '#size' => 26,
        '#maxlength' => 128,
        '#weight' => (1),
      );
    }
  }
}

function criteria_rating_votables()  {
  $r[1]['value']=20;
  $r[1]['label']=t('poor');
  $r[2]['value']=40;
  $r[2]['label']=t('OK');
  $r[3]['value']=60;
  $r[3]['label']=t('good');
  $r[4]['value']=80;
  $r[4]['label']=t('very good');
  $r[5]['value']=100;
  $r[5]['label']=t('excellent');
  return $r;
}

function criteria_rating_votingapi_get_vote_by_tag($nid, $tag, $uid) {
  $result = db_query("SELECT vote_id  FROM {votingapi_vote}  WHERE content_id = %d AND tag = '%s' AND uid = %d ", $nid, $tag, $uid);
  $vote = db_fetch_object($result);
  return $vote->vote_id;
}
  
  
/**
* Creates an average vote from tags (criteria) votes given by users, 
* which will be available for views.
* In most cases, this is the function that should be call before cron.php
*
*/
function _criteria_rating_table_update()  {
  $result = db_query("SELECT content_id from votingapi_vote GROUP BY content_id");
  while ($vobj = db_fetch_object($result)) {
    _criteria_rating_store_criteria_average('node', $vobj->content_id);    
  }
  drupal_set_message(t('Supplier Rating Overall has been re-calculated'));
  drupal_goto('admin');
  return;
}


function _criteria_rating_store_criteria_average($content_type, $content_id)  {
  if (is_numeric($content_id) == true   and  $content_type == 'node' )  {
    db_query("DELETE FROM {votingapi_vote} WHERE tag='tagsSUM' AND content_type = '%s' AND content_id = %d", $content_type, $content_id);
    $qry = "SELECT SUM(value)/COUNT(value)  GLOBAL_AVERAGE FROM votingapi_vote WHERE tag <>  'tagsSUM' AND content_type='".$content_type."' AND content_id ='".$content_id."'  ";
    $result = db_query($qry);
    $vote = db_fetch_object($result);
    $vobj->vote_id = db_next_id('{votingapi_vote}');
    $vobj->content_type = $content_type;
    $vobj->content_id = $content_id;
    $vobj->value = criteria_rating_roundnum($vote->GLOBAL_AVERAGE, 20);
    $vobj->value_type = 'percent';
    $vobj->tag = 'tagsSUM';
    $vobj->timestamp = time();
  
    $qry = " INSERT INTO {votingapi_vote} (vote_id, content_type, content_id, value, value_type, tag, timestamp, hostname) 
    VALUES (".$vobj->vote_id.", '".$vobj->content_type."', ".$vobj->content_id.", ".$vobj->value.", '".$vobj->value_type."', '".$vobj->tag."',  ".$vobj->timestamp.", '". $_SERVER["REMOTE_ADDR"] ."' ) ";
    db_query($qry);
    watchdog('criteria_rating', t('Average result created for node/'.$vobj->content_id  , array('@user' => 'system')));
  }
} 
 
function criteria_rating_user_overall($nid, $uid)  {
  $qry = "select count(value) as count ,  (sum(value)/count(value)) as overall  from  votingapi_vote WHERE content_id=".$nid." and uid = ".$uid."";
  $result = db_query($qry);
  $data = db_fetch_object($result);
  return $data;
}

function criteria_rating_user_tag_vote($nid, $tag)  {
  global $user;

  $qry = " select value from votingapi_vote WHERE  tag='".$tag."' AND uid=".$user->uid." AND content_id=".$nid." ";

  // echo "\$qry [$qry]<br/>";
  $result = db_query($qry);
  $data = db_fetch_object($result);
  
  //   echo print_r ($data);
  //   exit;
  return $data->value;
}

function criteria_rating_roundnum($num, $nearest){
  //from http://it.php.net/manual/en/function.round.php#83324
  return round($num / $nearest) * $nearest;
}